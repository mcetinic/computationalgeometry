﻿using System.Collections.Generic;
using UnityEngine;

public class SlowConvexHull : MonoBehaviour
{
    public int resolution = 10;
    public int minpointsLength = 1;
    public int maxpointsLength = 10;

    private Vector2[] randomPoints = null;

    private Vector2[][] linePointsSets;

    private List<Vector2[]> convexHullLines = new List<Vector2[]>();

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            randomPoints = GenerateNewPoligonPoints();

            linePointsSets = GetAllLinePointsSets(randomPoints.Length);

            convexHullLines = GetAllConvexHullEdges(randomPoints, linePointsSets);
        }
    }

    private Vector2[] GenerateNewPoligonPoints()
    {
        int pointsLength = Random.Range(minpointsLength, maxpointsLength);
        Vector2[] newPoints = new Vector2[pointsLength];

        for (int i = 0; i < pointsLength; i++)
        {
            newPoints[i] = Random.insideUnitCircle;
        }
        //Debug.Log("Generated Points Range: " + pointsLength);

        return newPoints;
    }

    private Vector2[][] GetAllLinePointsSets(int range)
    {
        int orderPairsNum = range * (range - 1);
        Vector2[][] pointsSets = new Vector2[orderPairsNum][];

        int orderPairsCount = 0;

        for (int i = 0; i < range; i++)
        {
            Vector2 p = randomPoints[i];

            for (int j = 0; j < range; j++)
            {
                if (i == j) continue;

                Vector2 q = randomPoints[j];

                pointsSets[orderPairsCount] = new Vector2[2];

                pointsSets[orderPairsCount][0] = p;
                pointsSets[orderPairsCount][1] = q;

                orderPairsCount++;
            }
        }

        return pointsSets;
    }

    private List<Vector2[]> GetAllConvexHullEdges(Vector2[] points, Vector2[][] linePointsSet)
    {
        List<Vector2[]> newConvexHullPoints = new List<Vector2[]>();

        int lineSetsLength = linePointsSet.Length;

        for (int i = 0; i < lineSetsLength; i++)
        {
            Vector2[] line = linePointsSet[i];

            Vector2 p = line[0];
            Vector2 q = line[1];

            bool isRight = true;

            int pointsLength = points.Length;
            for (int j = 0; j < pointsLength; j++)
            {
                Vector2 r = points[j];

                if (Vector3.Distance(r, p) <= 0 || Vector3.Distance(r, q) <= 0) continue;                

                if (IsLeft(p, q, r))
                {
                    isRight = false;
                    break;
                }
            }

            if (isRight) newConvexHullPoints.Add(line);
        }

        return newConvexHullPoints;
    }

    private bool IsLeft(Vector2 a, Vector2 b, Vector2 c)
    {
        return ((b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x)) > 0;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;

        float step = 2f / resolution;
        for (int i = 0; i <= resolution; i++)
        {
            ShowPoint(i * step - 1f, -1f);
            ShowPoint(i * step - 1f, 1f);
        }

        for (int i = 1; i < resolution; i++)
        {
            ShowPoint(-1f, i * step - 1f);
            ShowPoint(1f, i * step - 1f);
        }

        if (randomPoints != null)
        {
            for (int i = 0; i < randomPoints.Length; i++)
            {
                Gizmos.DrawSphere(randomPoints[i], 0.025f);
            }
        }

        Gizmos.color = Color.blue;

        if (linePointsSets != null)
        {
            int setsCount = linePointsSets.Length;

            for (int i = 0; i < setsCount; i++)
            {
                Vector3 p = linePointsSets[i][0];
                Vector3 q = linePointsSets[i][1];

                Gizmos.DrawLine(p, q);
                //Debug.Log("Points: " + poligonPointSets[i + j - 1][0] + " - " + poligonPointSets[i + j - 1][1]);              
            }
        }

        Gizmos.color = Color.red;

        if (convexHullLines != null)
        {
            int linesCount = convexHullLines.Count;

            for (int i = 0; i < linesCount; i++)
            {
                Vector2[] lineEdgePoints = convexHullLines[i];

                Gizmos.DrawLine(lineEdgePoints[0], lineEdgePoints[1]);
            }
        }
    }

    private void ShowPoint(float x, float y)
    {
        Vector2 square = new Vector2(x, y);
        Vector2 circle;
        circle.x = square.x * Mathf.Sqrt(1f - square.y * square.y * 0.5f);
        circle.y = square.y * Mathf.Sqrt(1f - square.x * square.x * 0.5f);

        Gizmos.color = Color.white;
        Gizmos.DrawSphere(circle, 0.015f);

        Gizmos.color = Color.green;
    }


}
